import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook {
  id: number
  name: string
  tel: string
  gender: string
}

export const useAddressBookStore = defineStore('address_book', () => {
  
  let lastId = 1
  const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  })
  const addressList = ref<AddressBook[]>([])
  const isAddNew = ref(false)
  function save() {
    // address.value.id = lastId++
    if (address.value.id > 0) {
      // Edit
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    } else { // Add New 
      addressList.value.push({ ...address.value, id: lastId++ })
    }
  
    address.value = {
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    }
  }
  
  function edit(id: number) {
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    // Copy Object JSON.parse(JSON.stringify(object))
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
  
  }
  
  function remove(id: number) {
    const removedIndex = addressList.value.findIndex((item) => item.id === id)
    // Copy Object JSON.parse(JSON.stringify(object))
    addressList.value.splice(removedIndex, 1)
  
  }
  return { address, addressList, save, isAddNew, edit, remove}
})
